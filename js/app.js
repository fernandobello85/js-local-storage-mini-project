const shop = document.getElementById('carrito');
const courses = document.getElementById('lista-cursos');
let shopList = document.querySelector('#lista-carrito tbody');
const dropShopListBtn = document.getElementById('vaciar-carrito');

loadEventListeners();

function loadEventListeners() {
    document.addEventListener('DOMContentLoaded', localStorageReady);
    courses.addEventListener('click', addListShop);
    shop.addEventListener('click', removeListShop);
    dropShopListBtn.addEventListener('click', dropShopList);
};


function addListShop(e) {
    e.preventDefault();
    if (e.target.classList.contains('agregar-carrito')) {
        const course = e.target.parentElement.parentElement;
        readCourse(course);
    }
};

function readCourse(course) {
    const infoCourse = {
        image: course.querySelector('img').src,
        title: course.querySelector('h4').textContent,
        price: course.querySelector('.precio span').textContent,
        id: course.querySelector('a').getAttribute('data-id')
    }
    insertCourse(infoCourse);
};

function createElement(course) {
    const row = document.createElement('tr');
    row.innerHTML = `
        <td>
            <img src= "${course.image}" width=100></img>
        </td>
        <td>${course.title}</td>
        <td>${course.price}</td>
        <td>
            <a href="#" class="borrar-curso" data-id = "${course.id}" >X</a>
        </td>
    `
    return row;
}
function insertCourse(course) {
    console.log(checkIfElementInArrary(course))
    if(checkIfElementInArrary(course) == false) {
        let element = createElement(course);
        shopList.appendChild(element);
        addElementToLS(course);
    }
};

function removeListShop(e) {
    e.preventDefault();
    if(e.target.className === 'borrar-curso') {
        course = e.target.parentElement.parentElement;
        course.remove();
        removeElementFromLS(course.querySelector('a').getAttribute('data-id'));
    }
};

function dropShopList() {
    while(shopList.firstChild) {
        shopList.removeChild(shopList.firstChild);
    }
    revemoveAllFromLS();
}

function retreiveElementsFromLS() {
    let elements;
    if( localStorage.getItem('lista-cursos') === null ) {
        elements = [];
    } else {
        elements = JSON.parse(localStorage.getItem('lista-cursos'));
    }
    return elements;
}

function addElementToLS(course) {
    let courses;
    courses = retreiveElementsFromLS();
    courses.push(course);
    localStorage.setItem('lista-cursos', JSON.stringify(courses));
}

function localStorageReady() {
    let elements;
    elements = retreiveElementsFromLS();
    elements.forEach(element => {
        let course = createElement(element);
        shopList.appendChild(course);
    });
}

function removeElementFromLS(element) {
    let elements;
    elements = retreiveElementsFromLS();
    elements = arrayRemoveElement(elements, element);
    localStorage.setItem('lista-cursos', JSON.stringify(elements));
}

function arrayRemoveElement(arr, value) {
    arr.forEach(function(element,idx) {
        if (element.id === value) {
            arr.splice(idx, 1);
        }
    })
    return arr
 }

 function revemoveAllFromLS() {
    let elements;
    elements = retreiveElementsFromLS();
    elements.splice(0, elements.length);
    localStorage.setItem('lista-cursos', JSON.stringify(elements));
 }

 function checkIfElementInArrary(e) {
     arr = retreiveElementsFromLS();
     let elementExists = false
     arr.forEach(function(element,idx) {
        if (element.id === e.id) {
            console.log('yes', e.id)
            elementExists = true
        }
    })
    return elementExists
 }

